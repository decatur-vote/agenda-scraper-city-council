# Meeting Agenda Scraper for Decatur, IL City Council 
This project is not ready to use.

This scraper supports scraping & parsing of [Granicus's](https://support.granicus.com/s/article/NovusAGENDA) NovusAgenda websites. In particular, it supports parsing Decatur, Illinois's [meetings & agendas](starting from https://decatur.novusagenda.com/agendapublic/meetingsgeneral.aspx)

## LICENSE
We have not yet decided what license to use. Probably MIT, but i don't know. We might add some restrictions for corporations while making it free for non-commercial purposes.
