# Status
For development notes & planning & stuff

## June 7, 2023
I'm now successfully scraping agenda item's cover sheets (tested only 1, but still). 

I'm also successfully downloading the file attachments listed in the agenda item's coversheet details.

Sadly, I'm re-downloading attachments every time, because the filename is determined by the headers.

I think my next step is to clean up the repo, wrap up the functionality, and make it simpler to do everything. Like, I want to call one function & download an entire agenda, parse everything, and get all its files too.

I Miiight want to convert all the separate JSONs into one single large JSON. Idk.

I also want to start figuring out the database layer.

And the viewer will come after that.


The json for an agenda item cover sheet is:
```json
{
    "Date": "2\/28\/2023",
    "Memo": "",
    "To": "Mayor Julie Moore Wolfe\r\nCity Council Members\n",
    "From": "Jon Kindseth, Deputy City Manager",
    "Subject": "Resolution Authorizing an Agreement between the Staley Family Foundation and the City of Decatur",
    "files": [
        {
            "name": "Memo staley",
            "url": "AttachmentViewer.ashx?AttachmentID=9555&ItemID=4244",
            "type": "Cover Memo"
        },
        {
            "name": "Resolution",
            "url": "AttachmentViewer.ashx?AttachmentID=9547&ItemID=4244",
            "type": "Resolution Letter"
        },
        {
            "name": "Agreement ",
            "url": "AttachmentViewer.ashx?AttachmentID=9546&ItemID=4244",
            "type": "Backup Material"
        }
    ]
}
```

## June 6, 2023
The new scraper is working well. I believe agendas are fully parsed now, including with video links.

I'm now working on parsing agenda items into structured data. This work is in `AgendaItemCoverPageParser.php` and `phptest GetAgendaItemFiles`.

I am already getting the header data with date, memo, to, and from.

I have yet to scrape the subject or the attachments. It looks like there are multiple `td[@id='column1']` ... and nested tables. The HTML structure is pretty awful. I'll just have to scour through the html & try selectors and just kinda brute force it until I get it working.

Structure I'm looking for, for an agenda item:
```json
{
    "agenda_url": "https://decatur.novusagenda.com/agendapublic/MeetingView.aspx?MeetingID=370&MinutesMeetingID=332&doctype=Agenda"
    "cover_url": "https://decatur.novusagenda.com/agendapublic/CoverSheet.aspx?ItemID=4244&MeetingID=370",
    "section": "VI",
    "sub_section": "1",
    "Date": "2/28/2023",
    "Memo": "text",
    "To": "Mayor Julie MOore Wolfe\nCity Council Members",
    "From": "Jon Kindseth, Deputy City Manager",
    "Subject": "Resolution Authorizing an Agreement between the Staley Family Foundation and the City of Decatur",
    "Attachments":{
        "Memo Staley":{
            "url": "https://decatur.novusagenda.com/agendapublic/AttachmentViewer.ashx?AttachmentID=9555&ItemID=4244",
            "type":"Cover Memo",
            "item_id": "4244",
            "file_id": "9555",
        },
        ... same for other two items
    }

}
```


## Jun 2, 2023  
I started the new scraper. It parses the list of meetings into structured data, downloads & parses agenda items into structured data & writes each of these things to disk. 

I added tests. The DownloadAndParseOneAgenda test is working as expected, though I haven't written the test-pass cases yet. I also may still modify the output a bit. I also need to test that the agenda parsing works with different agendas where some structures may be different and where additional items may be filled in.

I also probably need some way to track errors in the parsing, basically just to know if something got screwed up.

I also haven't considered video links ... 

Also: I am putting each parser in a trait, then the scraper can use that trait.
DecaturScraper & File are old. Scraper.php is new & good. Downloads.php is new and good

Work should continue in `src/CityCouncil/AgendaParser.php`.

Next is to start pulling documents from the links & storing those as well.

I'll also need to think about maybe storing this stuff in a database.

## June 1, 2023
Separated the scraper from the decaturvote.com monolith.

This project is a mess right now.

the Scraper is poorly organized - some functions do downloading AND parsing AND writing to disk. Some just do parsing and return an array. This all needs to be much improved & cleaner.

I need value objects for MeetingFiles (holding url & stuff) and for MeetingAgendas (holding detailed information about the agenda) and probably other value objects

I think my next step should be writing tests that do specific things, then re-organizing the code to accomodate. These tests should include:
- Download list of meetings, to create a simple index of available meetings
- Parse the list of meetings html into a structured format
- Write the list of meetings to disk
- Download all meetings from an index of available meetings
- Parse each meeting agenda into a structured format
- write the structured format of meeting agendas to disk
- Process a meeting agenda's structured data to get supplementary information (as html)
- Convert that html to structured data
- Write the supplementary information to disk

That's probably my starting point. I'll probably want a database to hold all this stuff, though. (large supplemental documents, however, should NOT go in the database, and probably don't really even need to be stored locally).



### Mar 27, 2023
This note retrieved from the decaturvote.com monolith repo. This (Mar 27, 2023) is the last time I worked on this:

added agenda scraper to tests. See `DecaturScraper.php` and test class `CityCouncilAgenda.php`. I'm currently working on `scrape_agenda`. I took good notes of the html structure of the agenda

I set up downloading of the meetings list (though downloading it manually lets me get a bigger list), converting the meetings list to a json array & writing that to disk, then downloading all of the agendas from that meeting list, and now I've started on scraping agendas
