<!-- DO NOT EDIT. This file generated from template by Code Scrawl https://tluf.me/php/code-scrawl/ -->  
# File src/DecaturScraper.php  
  
# class DecaturVote\CouncilAgendaScraper\Scraper  
A class for downloading & processing decatur, il city council meeting agendas.  
See source code at [/src/DecaturScraper.php](/src/DecaturScraper.php)  
  
## Constants  
  
## Properties  
  
## Methods   
- `public function download_agendas(string $json_meetings_file, string $out_dir)` Download ALL html agendas listed in the index file  
  
- `public function download_meeting(array $meeting, string $out_dir, bool $force_download = false): string` Download the agenda cover-page (html) for one meeting.  
  
- `public function meetings_list_to_json(string $meetings_file_path): string` Parse the html file for a list of meetings & convert it to a json array & write to disk   
  
- `public function download_file(\DecaturVote\CouncilAgendaScraper\File $file, string $output_dir): string` Simply download a file & write it to the output dir.   
  
- `public function get_meetings(string $html): array` Parse the html of a meetings list from novusagenda, convert it to an array of structured data, and return it.  
  
- `public function parse_agenda_cells(array $cells): array` Parse agenda html for all links, and return them in an array  
  
- `public function scrape_agenda(string $html): array` Parse the html of a meeting agenda and return an arary of structured content   
  
  
