<!-- DO NOT EDIT. This file generated from template by Code Scrawl https://tluf.me/php/code-scrawl/ -->  
# File src/Scraper.php  
  
# class DecaturVote\Scraper\CityCouncil  
  
See source code at [/src/Scraper.php](/src/Scraper.php)  
  
## Constants  
  
## Properties  
- `protected string $dir;` The working directory for downlodas & parsed output  
  
## Methods   
- `public function __construct(string $dir)`   
- `public function download(\DecaturVote\Scraper\CityCouncilDownloads $which_download, string $absolute_url = null, string $suffix''): string` Download a file, specified by $which_download. Will NOT download if file has been already downloaded today.  
  
- `public function file_path(string $file): string` Get the absolute path to a relative file  
- `public function write_file(string $file, string $content)`   
- `public function parse(\DecaturVote\Scraper\CityCouncilDownloads $which_download, string $absolute_file_path, string $suffix=''): array` Parse a file into structured json & write json to disk.  
  
  
