<?php

namespace DecaturVote\Scraper\CityCouncil;

trait AgendaItemCoverPageParser {

    public function parse_agenda_item_cover_page(string $absolute_file_path): array {
        $info = [];

        $html = file_get_contents($absolute_file_path);
        $doc = new \Taeluf\PHTML($html);

        $tables = $doc->xpath('//table//table//table');
        $header_table = $tables[0];
        $subject_table = $tables[1];
        $files_table = $tables[2];


        $header_doc = new \Taeluf\PHTML($header_table->__toString());
        $header_rows = (array)$header_doc->xpath('//tr');
        foreach ($header_rows as $tr){
            $tr = new \Taeluf\PHTML($tr->__toString());
            $tds = (array)$tr->xpath('//td');
            $key = $tds[0]->textContent;
            if (substr($key,-1)==':')$key = substr($key,0,-1);
            $key = ucfirst(strtolower($key));
            $value = $tds[1]->textContent;
            $info[$key] = $value;
        }


        $subject = trim(preg_replace('/[^a-zA-Z0-9\&\;\.\,\+\-\*\^\%\#\$\@\!\s]/','',str_replace("SUBJECT:",'',$subject_table->textContent)));


        $info['Subject'] = $subject;

        $files_doc = new \Taeluf\PHTML($files_table->__toString());
        $file_rows = (array)$files_doc->xpath('//tr');
        foreach ($file_rows as $tr){
            $tr = new \Taeluf\PHTML($tr->__toString());
            $tds = (array)$tr->xpath('//td');
            $a = ((array)$tds[0]->xpath('a'))[0] ?? null;
            if ($a == null)continue;
            $key = $tds[0]->textContent;
            if (substr($key,-1)==':')$key = substr($key,0,-1);
            $key = ucfirst(strtolower($key));
            $link = $a->href;
            $type = $tds[1]->textContent ?? null;
            $info['files'][] = [
                'name'=>$key,
                'url'=>$link,
                'type'=>$type,
            ];
        }

        return $info;
    }

}

