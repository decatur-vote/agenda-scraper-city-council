<?php

namespace DecaturVote\Scraper\CityCouncil;

trait AgendaParser {

    /**
     * Parse agenda html for all links, and return them in an array
     *
     * @param $cells I don't know
     * @return array of links, each being an array with key 'text' and 'link'
     */
    public function parse_agenda_cells(array $cells): array {

        $row = [];
        foreach ($cells as $cell){
            $text = trim($cell->innerText);
            $a_tags = $cell->xpath('a');
            $links = [];
            foreach ($a_tags as $a){
                $links[] = $a->href;
            }
            $row[] = ['text'=>$text, 'link'=>($links[0]??null)];
        }
        return $row;
    }

    public function parse_agenda(string $absolute_file_path): array {

        ###########
        #
        # WHEN TROUBLESHOOTING, uncomment `return $clean_items` at the end of this method.
        # That simply returns an array of all the rows containing all their non-empty columns, 
        # in order from first-to-last, with no structure.
        #
        ###########


        $html = file_get_contents($absolute_file_path);

        $url = \DecaturVote\Scraper\CityCouncilDownloads::AgendaPublic->url();

        $doc = new \Taeluf\PHTML($html);
        // $agenda_item_rows = $doc->xpath('//body/form/table/tbody/tr/td/table/tbody/tr');
        $agenda_item_rows = $doc->xpath('//tr');

        // var_dump($agenda_item_rows);

        // echo $html;

        $agenda_items = [];

        foreach ($agenda_item_rows as $agenda_item){
            if (trim($agenda_item->innerText) == '')continue;
            $agenda_items[] = $agenda_item;
        }
        // there are three nodes of little interest, as they are part of the header
        // I don't know if this changes with different meeting types
        array_shift($agenda_items);
        array_shift($agenda_items);
        $date_and_location = array_shift($agenda_items);

        $clean_items = [];
        $clean = [];
        // $last_section = 'error';
        // these are JUST the rows of interest
        $last_section_numeral = 'error';
        $last_subsection_number = 'error';
        foreach ($agenda_items as $agenda_item){
            $video_url = null;
            $columns = $agenda_item->children;


            $row = $this->parse_agenda_cells($agenda_item->children);
            // put parse_agenda_structure_old() HERE, if we decide to use it.

            if ($row[0]['text']=='VIDEO'){
                $video_col = array_shift($row);
                $row[] = $video_col;
            }

            $condensed_row = [];
            $first_text = null;
            foreach ($row as $index=>$col){
                $text = $col['text'] ?? $col['description'];
                $link = $col['link'];
                if (empty($text)&&empty($link))continue;
                if ($first_text==null)$first_text = $text;
                $condensed_row[] = 
                    ['text'=>$text, 'link'=>$link];
            }
            $row = $condensed_row;
            $row['first_text'] = $first_text;


            // NEW Parsing: 
            // My previous plan was to analyze which columns hold which information & use this to figure which bucket to put things into.
            // My new approach will determine WHAT we're dealing with by looking at the FIRST text encountered.
            // First text meaning:
            // ROMAN NUMERAL: A section, which contains child agenda items (possibly in multiple formats)
            // NUMBER: A sub-section. MAY be a single agenda item OR the consent calendar, which has multiple child items
            // Non-Roman, non-number, no link: Section Description? (policy relative to appearnce of citizens ... BIG PARAGRAPH)
            // Non-Roman, non-number, with link: Agenda item or addendum, like the Approval of Minutes and monthly reports.
            // A. B. C. etc: sub-item of a sub-section. Basically the items on the consent calendar.
            //
            // THEN
            // It then tracks the last numeral section & last numeric section & uses this information to properly structure the data


            $numerals = ['I','II','III','IV','V','VI','VII','VIII','IX','X','XI','XII','XIII','XIV','XV','XVI','XVII','XVIII','XIX','XX'];
            
            if (in_array($section_numeral=str_replace('.','',$first_text),$numerals)){
                // root sections denoted by a numeral
                $clean['section'][$section_numeral] = [
                    'numeral'=>$section_numeral,
                    'name'=>$row[1]['text'],
                    'video'=>$row[2]['link'] ?? '',
                    'items'=>[],
                ];
                $last_section_numeral = $section_numeral;
                $last_subsection_number = '-';
            } else if (is_numeric($section_number=str_replace('.','',$first_text))){
                //subsections: Agenda items denoted by a number
                $clean['section'][$last_section_numeral]['items'][$section_number] = [
                    'number'=>$section_number,
                    'description'=>$row[1]['text'],
                    'link'=>$row[1]['link'],
                    'video'=>$row[2]['link'] ?? '',
                ];
                $last_subsection_number = $section_number;
            } else if (preg_match('/^[A-Z]\.$/', $first_text)){
                // sub-section of an agenda item. Mainly consent calendar items
                $letter = str_replace('.','',$first_text);
                $clean['section'][$last_section_numeral]['items'][$last_subsection_number]['sub_items'][$letter] = 
                    [
                        'indice'=>$letter,
                        'description'=>$row[1]['text'],
                        'link'=>$row[1]['link'],
                        'video'=>$row[2]['link'] ?? '',
                    ];
            } else {
                // everything else, un-numbered subsection
                $clean['section'][$last_section_numeral]['items'][] = 
                    [
                        'description'=>$row[0]['text'],
                        'link'=>$row[0]['link'],
                        'video'=>$row[1]['link'] ?? '',

                    ];
            }




            $clean_items[] = $row;
            // $clean_items[] = $clean;
        }

        // print_r($clean_items);
        // print_r($agenda_items);

        // print_r($agenda_items);
        // echo implode("\n",$agenda_items);

        // return $clean_items;
        return $clean;

    }

    public function parse_agenda_structure_old(){
        $row =[];

            # determine which type of item it is. 
            # What are the cases?
            # case 1: SECTION col2 Roman numeral, col2 section title
            # case 2: AGENDA ITEM col2 empty, col3 list-number, col4 item description & NO link
            # case 3: AGENDA ITEM col2 empty, col3 empty, col4 item description & NO link (policy relative to appearance of citizens)
            # case 4: AGENDA ITEM col2 empty, col3 empty, col4 item description AND link (approval of minutes of March 6, 2023 ..., monthly reports)
            # case 5: AGENDA ITEM col2 empty, col3 list-number, col4 item description AND link
            # case 6: CONSENT CALENDAR ITEM col2 empty, col3 empty, col4 list-LETTER, col5 item description AND link


            ##
            # case 1 is top-level
            # case 2, 3, 4, 5 is child of SECTION
            # case 6 is child of AGENDA ITEM
            #
            # SECTION is top-level
            # AGENDA ITEM is child of SECTION
            # CONSENT CALENDAR ITEM is child of AGENDA ITEM 
                # CONSENT CALENDAR ITEM is ALSO an AGENDA ITEM, as far as data-structure & scraping goes 

            if ($row[1]['text']!=''){ // Case 1: a SECTION on the agenda
                $section_key = $row[1]['text'];
                $section_name = $row[2]['text'];
                $clean['section'][$section_key] = ['key'=>$section_key, 'name'=>$section_name];
                $last_section = $section_key;
            } else if (is_numeric(str_replace('.','',$row[2]['text'])) // remove periods
                && !empty($row[3]['text'])
                && empty($row[3]['link'])
            ){ //Case 2: a single agenda item WITHOUT a link
                $list_number = $row[2]['text'];
                $description = $row[3]['text'];
                $clean['section'][$last_section]['items'][$list_number] = [
                    'item'=>$list_number,
                    'description'=>$description,
                    'link'=>null,
                ];
            } else if ($row[2]['text'] == ''
                && !empty($row[3]['text'])
                && empty($row[3]['link'])
            ){ // Case 3: A single agenda item with no link and no list number
                $list_number = '-';
                $description = $row[3]['text'];
                $clean['section'][$last_section]['items'][$list_number] = [
                    'item'=>'-',
                    'description'=>$description,
                    'link'=>null,
                ];
            }
    }

}
