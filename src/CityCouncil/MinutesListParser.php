<?php

namespace DecaturVote\Scraper\CityCouncil;

trait MinutesListParser {

    /**
     * Parse the html from a meeting list & return a structured array of meetings.
     *
     * Contains keys date, type, location, agenda_url, minutes_url, and legal_minutes_url. 
     * minutes_url & legal_minutes url are null for new meetings
     *
     * @return array<int id, array meeting_info> each indice is an array of information about one particular meeting.
     *
     *
     */
    public function parse_meeting_list(string $absolute_file_path): array {
        $html = file_get_contents($absolute_file_path);
        $url = \DecaturVote\Scraper\CityCouncilDownloads::AgendaPublic->url();

        $doc = new \Taeluf\PHTML($html);
        $meeting_rows = $doc->xpath('//tbody/tr');

        /**
         * array of all meetings. Keys date,type,location,agenda_url,meeting_url
         */
        $meetings = [];

        foreach ($meeting_rows as $meeting_row){
            $c = 0;
            $cells = $meeting_row->childNodes;
            if (count($cells)!=9)continue;
            $date = $cells[1]->innerHTML;
            $type = $cells[2]->innerHTML;
            $loc = $cells[3]->innerHTML;
            $meeting = ['date'=>$date,'type'=> $type,'location'=> $loc];

            // get url for the agenda for this meeting
            $agendaScript = $cells[4]->childNodes[0]->childNodes[0]->onclick;
            $agenda = preg_replace('/.*(MeetingView\.aspx\?[^\']+).*/','$1',$agendaScript);
            $agendaUrl = $url.$agenda;
            $meeting['agenda_url'] = $agendaUrl;


            // get url for the meeting minutes
            $minutesScript = $cells[6]->childNodes[0]->childNodes[0]->onclick ?? null;
            if ($minutesScript==null){
                $minutesUrl = null;
            } else {
                $minutes = preg_replace('/.*(MeetingView\.aspx\?[^\']+).*/','$1',$minutesScript);
                $minutesUrl = $url.$minutes;
            }
            ##### echo "{$type} on {$date} at {$loc}:\n--{$agendaUrl}\n--{$minutesUrl}\n-------\n";
            /** MAY be null */
            $meeting['minutes_url'] = $minutesUrl;


            // $legalScript = $cells[7]->childNodes[0]->childNodes[0]->onclick ?? null;
            $legalUrl = $cells[7]->childNodes[0]->childNodes[0]->href ?? null;
            if ($legalUrl != null)$legalUrl = $url . $legalUrl;

            // if ($legalScript==null){
                // $legalUrl = null;
            // } else {
                // $legalUrl =
                // $legalUrl = $url.$legalUrl;
            // }
            $meeting['legal_minutes_url'] = $legalUrl;

            $meetings[] = $meeting;
        }

        return $meetings;
    }
}
