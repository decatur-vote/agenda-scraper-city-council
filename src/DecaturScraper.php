<?php

namespace DecaturVote\CouncilAgendaScraper;

############
##
#   OLD, MESSY, DISORGANIZED class. 
##
############

/**
 * A class for downloading & processing decatur, il city council meeting agendas.
 */
class Scraper {

    /**
     * Download ALL html agendas listed in the index file
     *
     * @TODO only download agendas that haven't already been downloaded
     *
     * @param $json_meetings_file 
     * @param $out_dir the directory to write to
     */
    public function download_agendas(string $json_meetings_file, string $out_dir){
        
        $meetings = json_decode(file_get_contents($json_meetings_file), true);
        foreach ($meetings as $m){
            echo "\nDownloading ". $m['date'];
            $this->download_meeting($m, $out_dir);
        }
        echo "\n";
    }

    /**
     * 
     * Download the agenda cover-page (html) for one meeting.
     *
     * @TODO make a value object for meeting
     *
     * @param $meeting Keys date,type,location,agenda_url,meeting_url
     * @param $out_dir the directory to write agendas to, like `PATH/output/agendas/`
     * @param $force_download set true to download even if the file already exists
     * 
     * @return full path to downloaded agenda html
     */
    public function download_meeting(array $meeting, string $out_dir, bool $force_download = false): string {

        $agenda_url = $meeting['agenda_url'];
        $date = str_replace('/','-',$meeting['date']);
        $agenda_out_dir = $out_dir.'/'.$date.'/';
        if (!is_dir($agenda_out_dir))mkdir($agenda_out_dir, 0754, true);
        $agenda_out_path = $agenda_out_dir.'/main.html';
        if ($force_download===false && file_exists($agenda_out_path))return $agenda_out_path;
        $agenda_content = file_get_contents($agenda_url);
        file_put_contents($agenda_out_path, $agenda_content);

        return $agenda_out_path;
    }

    /**
     * Parse the html file for a list of meetings & convert it to a json array & write to disk 
     *
     * @param $meetings_file_path The path of a downloaded html file that is the list of meetings from novusagenda
     *
     * @return full path to the json file that's generated
     */
    public function meetings_list_to_json(string $meetings_file_path): string {
        // $meetings_list_file = __DIR__.'/CityCouncilAgendas/output/meetings.html-Mar-27-2023.html';

        $dt = new \DateTime();
        $today = $dt->format("M-d-Y");
        $json_meetings_file = dirname($meetings_file_path).'/'.$today.'-meetings.json';
        $scraper = new \Dv\DecaturScraper();

        // echo 'zeep';
        // $Scraper->download_file(\Dv\File::MeetingList, __DIR__.'/CityCouncilAgendas/output/');

        $html = file_get_contents($meetings_file_path);
        $meetings = $scraper->get_meetings($html);

        $json = json_encode($meetings, JSON_PRETTY_PRINT);
        file_put_contents($json_meetings_file, $json);

        return $json_meetings_file;
    }


    /**
     * Simply download a file & write it to the output dir. 
     * 
     * @TODO review the output file name
     * @TODO don't download the same file twice
     * @TODO I don't think this works for any ol' file since the enum is very limited
     *
     * @param $file a file to download.
     * @param $output_dir the directory to download the file to.
     *
     * @return the full path to the output file
     */
    public function download_file(\DecaturVote\CouncilAgendaScraper\File $file, string $output_dir): string{
        $url = $file->url();
        // echo $url;
        // exit;
        $dt = new \DateTime();
        $today = $dt->format("M-d-Y");
        $out_path = $output_dir.'/'.$file->name().'-'.$today.'.html';

        $content = file_get_contents($url);

        file_put_contents($out_path, $content);

        return $out_path;
    }

    /**
     * Parse the html of a meetings list from novusagenda, convert it to an array of structured data, and return it.
     *
     * @param $html the html from File::MeetingList->url()
     * @return structured data representing each meeting.
     */
    public function get_meetings(string $html): array {

        $url = File::AgendaPublic->url();

        $doc = new \Taeluf\PHTML($html);
        $meeting_rows = $doc->xpath('//tbody/tr');

        /**
         * array of all meetings. Keys date,type,location,agenda_url,meeting_url
         */
        $meetings = [];

        foreach ($meeting_rows as $meeting_row){
            $c = 0;
            $cells = $meeting_row->childNodes;
            if (count($cells)!=9)continue;
            $date = $cells[1]->innerHTML;
            $type = $cells[2]->innerHTML;
            $loc = $cells[3]->innerHTML;
            $meeting = ['date'=>$date,'type'=> $type,'location'=> $loc];

            // get url for the agenda for this meeting
            $agendaScript = $cells[4]->childNodes[0]->childNodes[0]->onclick;
            $agenda = preg_replace('/.*(MeetingView\.aspx\?[^\']+).*/','$1',$agendaScript);
            $agendaUrl = $url.$agenda;
            $meeting['agenda_url'] = $agendaUrl;


            // get url for the meeting minutes
            $minutesScript = $cells[6]->childNodes[0]->childNodes[0]->onclick ?? null;
            if ($minutesScript==null){
                $minutesUrl = null;
            } else {
                $minutes = preg_replace('/.*(MeetingView\.aspx\?[^\']+).*/','$1',$minutesScript);
                $minutesUrl = $url.$minutes;
            }
            echo "{$type} on ${date} at ${loc}:\n--{$agendaUrl}\n--{$minutesUrl}\n-------\n";
            /** MAY be null */
            $meeting['minutes_url'] = $minutesUrl;


            // $legalScript = $cells[7]->childNodes[0]->childNodes[0]->onclick ?? null;
            $legalUrl = $cells[7]->childNodes[0]->childNodes[0]->href ?? null;
            if ($legalUrl != null)$legalUrl = $url . $legalUrl;

            // if ($legalScript==null){
                // $legalUrl = null;
            // } else {
                // $legalUrl =
                // $legalUrl = $url.$legalUrl;
            // }
            $meeting['legal_minutes_url'] = $legalUrl;

            $meetings[] = $meeting;




            continue;
            foreach ($meeting_row->childNodes as $td){
                echo ($c++).':'.($td->tagName??'null')."    ";
            }
            echo "\n-----\n";
            continue;
            $cn = $meeting_row->children;
            $td = $cn[0];
            // echo "\n######\n";
            // $tds = $doc->xpath('.//td', $meeting_row);
            $tdText = substr(trim($td->innerHTML),0,150);
            echo "\n---------\n";

            continue;
            $tds = $doc->xpath('//td',$meeting_row);
            $tdDate = $tds[0];
            echo $tdDate;
            echo "\n";
        }

        return $meetings;
    }


    /**
     * Parse agenda html for all links, and return them in an array
     *
     * @param $cells I don't know
     * @return array of links, each being an array with key 'text' and 'link'
     */
    public function parse_agenda_cells(array $cells): array {

        $row = [];
        foreach ($cells as $cell){
            $text = trim($cell->innerText);
            $a_tags = $cell->xpath('a');
            $links = [];
            foreach ($a_tags as $a){
                $links[] = $a->href;
            }
            $row[] = ['text'=>$text, 'link'=>($links[0]??null)];
        }
        return $row;
    }

    /**
     * Parse the html of a meeting agenda and return an arary of structured content 
     *
     * @param $html the html for a meeting agenda
     * @return array of structured content representing the agenda
     */
    public function scrape_agenda(string $html): array {

        $url = File::AgendaPublic->url();

        $doc = new \Taeluf\PHTML($html);
        // $agenda_item_rows = $doc->xpath('//body/form/table/tbody/tr/td/table/tbody/tr');
        $agenda_item_rows = $doc->xpath('//tr');

        var_dump($agenda_item_rows);

        // echo $html;



        $agenda_items = [];

        foreach ($agenda_item_rows as $agenda_item){
            if (trim($agenda_item->innerText) == '')continue;
            $agenda_items[] = $agenda_item;
        }
        // there are three nodes of little interest, as they are part of the header
        // I don't know if this changes with different meeting types
        array_shift($agenda_items);
        array_shift($agenda_items);
        $date_and_location = array_shift($agenda_items);

        $clean_items = [];
        // these are JUST the rows of interest
        foreach ($agenda_items as $agenda_item){
            $video_url = null;
            $columns = $agenda_item->children;


            $row = $this->parse_agenda_cells($agenda_item->children);
            // $clean_items[] = $row;

            # determine which type of item it is. 
            # What are the cases?
            # case 1: SECTION col2 Roman numeral, col2 section title
            # case 2: AGENDA ITEM col2 empty, col3 list-number, col4 item description & NO link
            # case 3: AGENDA ITEM col2 empty, col3 empty, col4 item description & NO link (policy relative to appearance of citizens)
            # case 4: AGENDA ITEM col2 empty, col3 empty, col4 item description AND link (approval of minutes of March 6, 2023 ..., monthly reports)
            # case 5: AGENDA ITEM col2 empty, col3 list-number, col4 item description AND link
            # case 6: CONSENT CALENDAR ITEM col2 empty, col3 empty, col4 list-LETTER, col5 item description AND link


            ##
            # case 1 is top-level
            # case 2, 3, 4, 5 is child of SECTION
            # case 6 is child of AGENDA ITEM
            #
            # SECTION is top-level
            # AGENDA ITEM is child of SECTION
            # CONSENT CALENDAR ITEM is child of AGENDA ITEM 
                # CONSENT CALENDAR ITEM is ALSO an AGENDA ITEM, as far as data-structure & scraping goes 

        }

        print_r($clean_items);

        // print_r($agenda_items);
        // echo implode("\n",$agenda_items);

        return $agenda_items;

        /**
         * array of all meetings. Keys date,type,location,agenda_url,meeting_url
         */
        $meetings = [];

        foreach ($meeting_rows as $meeting_row){
            $c = 0;
            $cells = $meeting_row->childNodes;
            if (count($cells)!=9)continue;
            $date = $cells[1]->innerHTML;
            $type = $cells[2]->innerHTML;
            $loc = $cells[3]->innerHTML;
            $meeting = ['date'=>$date,'type'=> $type,'location'=> $loc];

            // get url for the agenda for this meeting
            $agendaScript = $cells[4]->childNodes[0]->childNodes[0]->onclick;
            $agenda = preg_replace('/.*(MeetingView\.aspx\?[^\']+).*/','$1',$agendaScript);
            $agendaUrl = $url.$agenda;
            $meeting['agenda_url'] = $agendaUrl;


            // get url for the meeting minutes
            $minutesScript = $cells[6]->childNodes[0]->childNodes[0]->onclick ?? null;
            if ($minutesScript==null){
                $minutesUrl = null;
            } else {
                $minutes = preg_replace('/.*(MeetingView\.aspx\?[^\']+).*/','$1',$minutesScript);
                $minutesUrl = $url.$minutes;
            }
            echo "{$type} on ${date} at ${loc}:\n--{$agendaUrl}\n--{$minutesUrl}\n-------\n";
            /** MAY be null */
            $meeting['minutes_url'] = $minutesUrl;


            // $legalScript = $cells[7]->childNodes[0]->childNodes[0]->onclick ?? null;
            $legalUrl = $cells[7]->childNodes[0]->childNodes[0]->href ?? null;
            if ($legalUrl != null)$legalUrl = $url . $legalUrl;

            // if ($legalScript==null){
                // $legalUrl = null;
            // } else {
                // $legalUrl =
                // $legalUrl = $url.$legalUrl;
            // }
            $meeting['legal_minutes_url'] = $legalUrl;

            $meetings[] = $meeting;




            continue;
            foreach ($meeting_row->childNodes as $td){
                echo ($c++).':'.($td->tagName??'null')."    ";
            }
            echo "\n-----\n";
            continue;
            $cn = $meeting_row->children;
            $td = $cn[0];
            // echo "\n######\n";
            // $tds = $doc->xpath('.//td', $meeting_row);
            $tdText = substr(trim($td->innerHTML),0,150);
            echo "\n---------\n";

            continue;
            $tds = $doc->xpath('//td',$meeting_row);
            $tdDate = $tds[0];
            echo $tdDate;
            echo "\n";
        }

        return $meetings;
    }
}


