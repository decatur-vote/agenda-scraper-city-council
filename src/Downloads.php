<?php

namespace DecaturVote\Scraper;

enum CityCouncilDownloads {
    
    /**
     * The meeting list, but it's the iframe url 
     */
    case MeetingList;
    /**
     * file:
     * It is also the prefix for urls to individual agendas 
     */
    case AgendaPublic;
    case LegalMinutesPdf;
    case AgendaItemCoverPage;
    case AgendaItemFile;

    public function url(): string {
        return match ($this)
        {
            static::MeetingList => "https://decatur.novusagenda.com/agendapublic/meetingsgeneral.aspx",
            static::AgendaPublic => "https://decatur.novusagenda.com/agendapublic/",
            /** yes, it says Agenda, but it's the legal minutes. It takes a `?MeetingMinutesId=int` */
            static::LegalMinutesPdf => "https://decatur.novusagenda.com/agendapublic/DisplayAgendaPDF.ashx",
            static::AgendaItemCoverPage => 'https://decatur.novusagenda.com/agendapublic/',
            static::AgendaItemFile => 'https://decatur.novusagenda.com/agendapublic/',
        };
    }

    public function name(): string {
        return match ($this)
        {
            static::MeetingList => "meetings",
            static::AgendaPublic => "agendas",
            static::LegalMinutesPdf => "legal-pdf",
            static::AgendaItemCoverPage => 'agenda-item',
            static::AgendaItemFile => 'agenda-attachment',
        };
    }

}
