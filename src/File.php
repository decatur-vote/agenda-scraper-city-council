<?php

namespace DecaturVote\CouncilAgendaScraper;

enum File {
    
    /**
     * The meeting list, but it's the iframe url 
     */
    case MeetingList;
    /**
     * file:
     * It is also the prefix for urls to individual agendas 
     */
    case AgendaPublic;
    case LegalMinutesPdf;

    public function url(): string {
        return match ($this)
        {
            File::MeetingList => "https://decatur.novusagenda.com/agendapublic/meetingsgeneral.aspx",
            File::AgendaPublic => "https://decatur.novusagenda.com/agendapublic/",
            /** yes, it says Agenda, but it's the legal minutes. It takes a `?MeetingMinutesId=int` */
            File::LegalMinutesPdf => "https://decatur.novusagenda.com/agendapublic/DisplayAgendaPDF.ashx",
        };
    }

    public function name(): string {
        return match ($this)
        {
            File::MeetingList => "meetings",
            File::AgendaPublic => "agendas",
            File::LegalMinutesPdf => "legal-pdf",
        };
    }

}
