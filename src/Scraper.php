<?php

namespace DecaturVote\Scraper;

class CityCouncil {

    use CityCouncil\MinutesListParser;
    use CityCouncil\AgendaParser;
    use CityCouncil\AgendaItemCoverPageParser;

    /** The working directory for downlodas & parsed output */
    protected string $dir;

    /** Maximum filesize to download in bytes. Defaults to 1MB */
    public int $max_filesize = 1024 * 1024;

    public function __construct(string $dir){
        $this->dir = $dir;
        if (!is_dir($dir)){
            throw new \Exception("Directory '$dir' does not exist.");
        }
    }

    /**
     * Download a file, specified by $which_download. Will NOT download if file has been already downloaded today.
     *
     * @param $which_download which kind of file to download
     * @param $absolute_url the absolute url to an item
     * @param $suffix a suffix to append to the file name (prior to the extension)
     *
     * @return absolute path to the downloaded file
     */
    public function download(\DecaturVote\Scraper\CityCouncilDownloads $which_download, ?string $absolute_url = null, string $suffix=''): string {
        $url = $which_download->url();

        if ($suffix!='')$suffix = '-'.$suffix;
        $file = $which_download->name().$suffix.'.html';

        if (is_file($full_path=$this->file_path($file))){
            return $full_path;
        }

        if (!is_null($absolute_url))$url = $absolute_url;

        // $content = file_get_contents($url);
        $content = $this->curl_download($url, $extension);

        $file = trim($which_download->name().$suffix).'.'.($extension ?? 'html');

        return $this->write_file($file, $content);

    }

    /**
     * Get the absolute path to a relative file
     * @param $file the relative path to a file.
     * @return absolute path, with the root dir and a dirname for today's date prepended.
     */
    public function file_path(string $file): string {
        $dt = new \DateTime();
        $today = $dt->format("M-d-Y");

        $out_dir = $this->dir.'/'.$today.'/';
        $out_path = $out_dir.$file;

        return $out_path;
    }

    /**
     * Write file to disk and return absolue file path
     * @param $file file name
     * @param $content content to write to disk
     * @return absolute file path
     */
    public function write_file(string $file, string $content): string {
        $out_path = $this->file_path($file);

        if (file_exists($out_path)){
            return $out_path;
        }

        if (!is_dir(dirname($out_path)))mkdir(dirname($out_path), 0754, true);

        file_put_contents($out_path, $content);
        return $out_path;
    }

    /**
     * Parse a file into structured json & write json to disk.
     *
     * @param $which_download which kind of file parse
     * @param $absolute_file_path Path to the file to parse (typically the path is the return value from a call to download())
     * @param $suffix a suffix to append to the file name (prior to the extension)
     *
     * @return array of structured data from parsing.
     */
    public function parse(\DecaturVote\Scraper\CityCouncilDownloads $which_download, string $absolute_file_path, string $suffix=''): array {
        
        $parsed = match ($which_download)
        {
            $which_download::MeetingList => $this->parse_meeting_list($absolute_file_path),
            $which_download::AgendaPublic => $this->parse_agenda($absolute_file_path),
            $which_download::LegalMinutesPdf => $this->parse_legal_minutes_pdf($absolute_file_path),
            $which_download::AgendaItemCoverPage => $this->parse_agenda_item_cover_page($absolute_file_path),
        };

        $json = json_encode($parsed, JSON_PRETTY_PRINT);
        if ($suffix!='')$suffix = '-'.$suffix;
        $file = $which_download->name().$suffix.'.json';
        $this->write_file($file,$json);


        return $parsed;
    }

    /**
     * Download a file using cURL over GET, determining the file extension based on the MIME type in the response headers.
     *
     * @param string $url The URL of the file to download.
     * @param string|null $suffix The desired file suffix (optional).
     * @return ?string absolute file path or null on failure
     */
    function curl_download(string $url, ?string &$extension): ?string {


        ## the headers don't seem to include the content length! 
        # So let's not make a request that we're doing nothing with.
        // $file_size = $this->curl_get_filesize($url);
        // if ($file_size > $this->max_filesize) { // 1MB
            // $extension = 'txt';
            // return 'File too large to download. Download file from '.$url;
        // }

        $ch = curl_init($url);

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HEADER, true);

        $response = curl_exec($ch);

        if ($response === false) {
            // echo 'Error: ' . curl_error($ch);
            return null;
        }

        $headerSize = curl_getinfo($ch, CURLINFO_HEADER_SIZE);
        $headers = substr($response, 0, $headerSize);
        $body = substr($response, $headerSize);

        curl_close($ch);

        $mime = '';
        preg_match('/Content-Type: (.+?)\s/', $headers, $matches);
        $mime = $matches[1] ?? null;

        // preg_match('/Content-Disposition: attachment;filename=([^;]+)/', $headers, $matches);
        // $filename = $matches[1] ?? null;

        $extensions = [
            'image/jpeg' => 'jpg',
            'image/png' => 'png',
            'application/pdf' => 'pdf',
            // Add more MIME types and extensions as needed
        ];

        $extension = isset($extensions[$mime]) ? $extensions[$mime] : 'txt';

        return $body;
    }

    /**
     * Get the size of a file download without actually downloading the file.
     *
     * @param string $url The URL of the file to get the size for.
     * @return int The size of the file in bytes, or -1 on failure. 
     */
    function curl_get_filesize(string $url): int {
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_NOBODY, true); // Fetch header only, no body
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HEADER, true);

        $response = curl_exec($ch);

        // var_dump($response);

        if ($response === false) {
            return -1;
        }

        $filesize = (int)curl_getinfo($ch, CURLINFO_CONTENT_LENGTH_DOWNLOAD);

        curl_close($ch);

        return $filesize;
    }

}
