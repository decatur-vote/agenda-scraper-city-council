<?php
return;


$url = 'https://decatur.novusagenda.com/agendapublic/meetingsgeneral.aspx';

$content = file_get_contents($url);

$dt = new DateTime();
$today = $dt->format("M-d-Y");

file_put_contents(__DIR__.'/output/agenda-'.$today.'.html', $content);

// https://decatur.novusagenda.com/agendapublic/meetingsgeneral.aspx
