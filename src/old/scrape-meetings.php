<?php
return;

ini_set('display_errors', 1);
error_reporting(E_ALL);

require_once(__DIR__.'/../../depend.php');

$html = file_get_contents(__DIR__.'/output/agenda-Sep-05-2020.html');
$doc = new \Taeluf\PHTML($html);
$entries = $doc->xpath('//tbody/tr');
// var_dump($entries[0]);
foreach ($entries as $tr){
    $c = 0;
    $children = $tr->childNodes;
    if (count($children)!=9)continue;
    $date = $children[1]->innerHTML;
    $type = $children[2]->innerHTML;
    $loc = $children[3]->innerHTML;

    $url = 'https://decatur.novusagenda.com/agendapublic/';

    $agendaScript = $children[4]->childNodes[0]->childNodes[0]->onclick;
    $agenda = preg_replace('/.*(MeetingView\.aspx\?[^\']+).*/','$1',$agendaScript);
    $agendaUrl = $url.$agenda;

    $minutesScript = $children[6]->childNodes[0]->childNodes[0]->onclick ?? null;
    if ($minutesScript==null){
        $minutesUrl = 'not-available';
    } else {
        $minutes = preg_replace('/.*(MeetingView\.aspx\?[^\']+).*/','$1',$minutesScript);
        $minutesUrl = $url.$minutes;
    }
    echo "{$type} on ${date} at ${loc}:\n--{$agendaUrl}\n--{$minutesUrl}\n-------\n";

    
    // echo $date."\n";
    continue;
    foreach ($tr->childNodes as $td){
        echo ($c++).':'.($td->tagName??'null')."    ";
    }
    echo "\n-----\n";
    continue;
    $cn = $tr->children;
    $td = $cn[0];
    // echo "\n######\n";
    // $tds = $doc->xpath('.//td', $tr);
    $tdText = substr(trim($td->innerHTML),0,150);
    echo "\n---------\n";

    continue;
    $tds = $doc->xpath('//td',$tr);
    $tdDate = $tds[0];
    echo $tdDate;
    echo "\n";
}
