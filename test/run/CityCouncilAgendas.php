<?php

namespace DecaturVote\CityCouncilScraper;

class CityCouncilAgendas extends \Tlf\Tester {

    public function testGetAgendaItemFiles(){
        $scraper = new \DecaturVote\Scraper\CityCouncil($this->file('test/output/CityCouncil/'));
        $scraper->max_filesize = 1024;
        $details_file = $this->file('test/expect/03_06_23_Agenda_VI.1.json');
        $info = json_decode(file_get_contents($details_file), true);

        $dir = null;

        foreach ($info['files'] as $file){
            $name = $file['name'];
            $type = $file['type'];
            $url = $file['url'];
            $url = \DecaturVote\Scraper\CityCouncilDownloads::AgendaItemFile->url().$url;
            $file = $scraper->download(
                \DecaturVote\Scraper\CityCouncilDownloads::AgendaItemFile,
                $url,
                $name,
            );
            $dir = dirname($file);
        }

        // $prefix = $this->file('test/output/CityCouncil/agenda-attachment-');
        $prefix = $dir.'/agenda-attachment-';
        $this->file_exists($prefix.'Agreement.pdf');
        $this->file_exists($prefix.'Memo staley.pdf');
        $this->file_exists($prefix.'Resolution.pdf');
    }

    public function testParseAgendaItemCoverSheet(){
        $scraper = new \DecaturVote\Scraper\CityCouncil($this->file('test/output/CityCouncil/'));

        // same agenda we're using for testDownloadAndParseOneAgenda
        $agenda_url = "https://decatur.novusagenda.com/agendapublic/MeetingView.aspx?MeetingID=370&MinutesMeetingID=332&doctype=Agenda";

        $clean_date = '03_06_23';
        $agenda_file = 
            $scraper->download(
                \DecaturVote\Scraper\CityCouncilDownloads::AgendaPublic, 
                $agenda_url,
                $clean_date
            );

        $agenda_details = $scraper->parse(
            \DecaturVote\Scraper\CityCouncilDownloads::AgendaPublic,
            $agenda_file,
            $clean_date
        );

        $item_link = $agenda_details['section']['VI']['items'][1]['link'];
        $file_suffix = $clean_date.'_VI.1';

        $item_url = \DecaturVote\Scraper\CityCouncilDownloads::AgendaItemCoverPage->url().$item_link;
        $cover_sheet_file = $scraper->download(
            \DecaturVote\Scraper\CityCouncilDownloads::AgendaItemCoverPage,
            $item_url,
            $file_suffix,  
        );

        $details = $scraper->parse(
            \DecaturVote\Scraper\CityCouncilDownloads::AgendaItemCoverPage,
            $cover_sheet_file,
            $file_suffix,
        );

        $expect_file = $this->file('test/expect/03_06_23_Agenda_VI.1.json');
        $expect = json_decode(file_get_contents($expect_file), true);

        $this->compare_arrays($expect, $details);
        
    }


    public function testGetListOfMeetings(){
        $scraper = new \DecaturVote\Scraper\CityCouncil($this->file('test/output/CityCouncil/'));

        $file = $scraper->download(\DecaturVote\Scraper\CityCouncilDownloads::MeetingList);

        $meeting_list = $scraper->parse(\DecaturVote\Scraper\CityCouncilDownloads::MeetingList, $file);

        $meeting = $meeting_list[0];
        echo "\n\nFirst meeting in list, (should be most recent):\n";
        print_r($meeting);
        echo "\n\n\n-----------\n\n";

        $this->test("Meeting info returns expectd data.");
        
        $this->test('meeting->date is DateTime parseable `m/d/y`');
        $date = \DateTime::createFromFormat('m/d/y', $meeting['date']);
        $this->compare(\DateTime::class, get_class($date));

        $this->test('meeting->type & meeting->location are strings');
        $this->is_true(is_string($meeting['type']));
        $this->is_true(is_string($meeting['location']));

        $this->test('`meeting->agenda_url`\'s PHP_URL_HOST is decatur.novusagenda.com');
        $agenda_host = parse_url($meeting['agenda_url'], PHP_URL_HOST);

        $this->compare('decatur.novusagenda.com', $agenda_host);

    }

    /**
     * Specifically tests the March 6, 2023 meeting minutes. I manually verified the output, then wrote it to disk & now programmitcally make sure the output is the same.
     */
    public function testDownloadAndParseOneAgenda(){
        $scraper = new \DecaturVote\Scraper\CityCouncil($this->file('test/output/CityCouncil/'));

        // $meeting_file = $scraper->download(\DecaturVote\Scraper\CityCouncilDownloads::MeetingList);

        // $meeting_list = $scraper->parse(\DecaturVote\Scraper\CityCouncilDownloads::MeetingList, $meeting_file);

        // $latest_meeting = $meeting_list[0];
        // echo "\n\nFirst meeting in list, (should be most recent):\n";
        // print_r($latest_meeting);
        // echo "\n\n\n-----------\n\n";


        $agenda_url = "https://decatur.novusagenda.com/agendapublic/MeetingView.aspx?MeetingID=370&MinutesMeetingID=332&doctype=Agenda";

        // $clean_date = str_replace('/', '-',$latest_meeting['date']);
        $clean_date = '03_06_23';
        $agenda_file = 
            $scraper->download(
                \DecaturVote\Scraper\CityCouncilDownloads::AgendaPublic, 
                $agenda_url,
                $clean_date
            );

        $agenda_details = $scraper->parse(
            \DecaturVote\Scraper\CityCouncilDownloads::AgendaPublic,
            $agenda_file,
            $clean_date
        );


        $expect_file = $this->file('test/expect/03_06_23_Minutes.json');
        $expect = json_decode(file_get_contents($expect_file), true);

        $this->compare_arrays($expect, $agenda_details);

    }
}
